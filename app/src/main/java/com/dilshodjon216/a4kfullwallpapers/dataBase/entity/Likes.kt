package com.dilshodjon216.a4kfullwallpapers.dataBase.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "likes")
data class Likes(
    @PrimaryKey(autoGenerate = true) val uid: Int=0,
    @ColumnInfo(name = "imagesUrl") val imagesUrl: String?,
    @ColumnInfo(name = "width")val with:String,
    @ColumnInfo(name = "height")val height:String,
    @ColumnInfo(name = "photographer")val photographer:String,
    @ColumnInfo(name = "photographer_url")val photographer_url:String
)