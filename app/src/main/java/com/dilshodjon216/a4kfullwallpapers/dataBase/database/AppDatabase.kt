package com.dilshodjon216.a4kfullwallpapers.dataBase.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dilshodjon216.a4kfullwallpapers.dataBase.Dao.LikesDao
import com.dilshodjon216.a4kfullwallpapers.dataBase.entity.Likes


@Database(entities = [Likes::class],version = 1,exportSchema = false)
abstract class AppDatabase :RoomDatabase(){
    abstract fun  likesDao():LikesDao


}