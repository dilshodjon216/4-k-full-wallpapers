package com.dilshodjon216.a4kfullwallpapers.dataBase.Dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.dilshodjon216.a4kfullwallpapers.dataBase.entity.Likes


@Dao
interface LikesDao{
    @Query("SELECT * FROM likes")
    fun getAll():List<Likes>
    @Insert
    fun insertLikes(likes: Likes)

    @Delete
    fun deleteLikes(likes: Likes)
    @Query("SELECT *FROM likes where  imagesUrl=:imagesUrl")
    fun getLikesPhoto(imagesUrl:String):Likes


}