package com.dilshodjon216.a4kfullwallpapers.activtiy

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.fragment.ImageFragment

class ImageActivity : AppCompatActivity() {
    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_image)


        var image = intent.getStringExtra("imageLarge")
        var author = intent.getStringExtra("author")
        var url = intent.getStringExtra("url")
        var width = intent.getStringExtra("width")
        var height = intent.getStringExtra("height")
        var imageOrginal = intent.getStringExtra("imageOrginal")
        var imagePortarit = intent.getStringExtra("imagePortarit")


        var imageFragment = ImageFragment()

        var bundle = Bundle()
        bundle.putString("image", image)
        bundle.putString("author", author)
        bundle.putString("url", url)
        bundle.putString("width", width)
        bundle.putString("height", height)
        bundle.putString("imageOrginal", imageOrginal)
        bundle.putString("imagePortarit", imagePortarit)

        imageFragment.arguments = bundle

        supportFragmentManager.beginTransaction().add(R.id.image_contener, imageFragment, "")
            .commit()


//
//
//        Glide.with(this).load(image).placeholder(R.drawable.img).into(imageBackground)
//
//
//        image_info.setOnClickListener {
//
//        }
//
//        image_shareit.setOnClickListener {
//
//        }
//
//        imge_exit.setOnClickListener {
//            finish()
//        }
    }
}