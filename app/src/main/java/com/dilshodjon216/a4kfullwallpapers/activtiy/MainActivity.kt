package com.dilshodjon216.a4kfullwallpapers.activtiy

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.fragment.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    lateinit var toggle: ActionBarDrawerToggle
    lateinit var menuBottom_home: MenuItem
    lateinit var menuBottom_popular: MenuItem
    lateinit var menuBottom_random: MenuItem
    lateinit var menuBottom_liked: MenuItem
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigation.setOnNavigationItemSelectedListener(selectedListener)

        val menu = navigation.menu
        menuBottom_home = menu.findItem(R.id.menuBottom_home)
        menuBottom_popular = menu.findItem(R.id.menuBottom_popular)
        menuBottom_random = menu.findItem(R.id.menuBottom_random)
        menuBottom_liked = menu.findItem(R.id.menuBottom_liked)


        var homeFragment = HomeFragment()
        supportFragmentManager.beginTransaction().add(R.id.mainContener, homeFragment, "")
            .commit()
        navmenu.setCheckedItem(R.id.menu_home)
        menuBottom_home.setIcon(R.drawable.ic_home)

        search.setOnClickListener {
            appTitle.visibility = View.INVISIBLE
            exit_sreach.visibility = View.VISIBLE
            search.visibility=View.INVISIBLE
            searchET.visibility = View.VISIBLE
            searchET.setText("")

            var homeFragment = SearchFragment()
            supportFragmentManager.beginTransaction().replace(R.id.mainContener, homeFragment, "")
                .addToBackStack(homeFragment.toString())
                .commit()
        }

        exit_sreach.setOnClickListener {
            appTitle.visibility = View.VISIBLE
            exit_sreach.visibility = View.INVISIBLE
            search.visibility=View.VISIBLE
            searchET.visibility = View.INVISIBLE

            var homeFragment = HomeFragment()
            supportFragmentManager.beginTransaction().replace(R.id.mainContener, homeFragment, "")
                .addToBackStack(homeFragment.toString())
                .commit()
        }



        navmenu.setNavigationItemSelectedListener(object :
            NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {

                when (item.itemId) {
                    R.id.menu_home -> {
                        appTitle.text = "Home"
                        var homeFragment = HomeFragment()
                        supportFragmentManager.beginTransaction().replace(
                            R.id.mainContener,
                            homeFragment,
                            ""
                        )
                            .commit()
                        search.visibility = View.VISIBLE
                        random.visibility = View.INVISIBLE

                        menuBottom_home.setIcon(R.drawable.ic_home)
                        menuBottom_popular.setIcon(R.drawable.ic_popular1)
                        menuBottom_random.setIcon(R.drawable.ic_random)
                        menuBottom_liked.setIcon(R.drawable.ic_liked1)


                        navigation.selectedItemId = R.id.menuBottom_home;
                        navmenu.setCheckedItem(R.id.menu_home)
                        drawer.closeDrawer(GravityCompat.START)
                    }
                    R.id.menu_popular -> {

                        appTitle.text = "Popular"
                        var popularFragment = PopularFragment()
                        supportFragmentManager.beginTransaction().replace(
                            R.id.mainContener,
                            popularFragment,
                            ""
                        ).commit()

                        navigation.selectedItemId = R.id.menuBottom_popular;
                        navmenu.setCheckedItem(R.id.menu_popular)

                        menuBottom_home.setIcon(R.drawable.ic_home1)
                        menuBottom_popular.setIcon(R.drawable.ic_popular)
                        menuBottom_random.setIcon(R.drawable.ic_random)
                        menuBottom_liked.setIcon(R.drawable.ic_liked1)

                        search.visibility = View.VISIBLE
                        random.visibility = View.INVISIBLE
                        drawer.closeDrawer(GravityCompat.START)
                    }
                    R.id.menu_random -> {

                        appTitle.text = "Random"
                        var randomFragment = RandomFragment()
                        supportFragmentManager.beginTransaction().replace(
                            R.id.mainContener,
                            randomFragment,
                            ""
                        ).commit()

                        menuBottom_home.setIcon(R.drawable.ic_home1)
                        menuBottom_popular.setIcon(R.drawable.ic_popular1)
                        menuBottom_random.setIcon(R.drawable.ic_random1)
                        menuBottom_liked.setIcon(R.drawable.ic_liked1)

                        drawer.closeDrawer(GravityCompat.START)
                        navigation.selectedItemId = R.id.menuBottom_random;
                        navmenu.setCheckedItem(R.id.menu_random)
                        search.visibility = View.INVISIBLE
                        random.visibility = View.VISIBLE
                    }
                    R.id.menu_liked -> {
                        appTitle.text = "My Favourites"
                        var favouritesFragment = MyFavouritesFragment()
                        supportFragmentManager.beginTransaction().replace(
                            R.id.mainContener,
                            favouritesFragment,
                            ""
                        ).commit()
                        navigation.selectedItemId = R.id.menuBottom_liked;
                        navmenu.setCheckedItem(R.id.menu_liked)
                        search.visibility = View.INVISIBLE
                        random.visibility = View.INVISIBLE
                        menuBottom_home.setIcon(R.drawable.ic_home1)
                        menuBottom_popular.setIcon(R.drawable.ic_popular1)
                        menuBottom_random.setIcon(R.drawable.ic_random)
                        menuBottom_liked.setIcon(R.drawable.ic_liked)
                        drawer.closeDrawer(GravityCompat.START)
                    }
                    R.id.menu_history -> {
                        appTitle.text = "History"
                        Toast.makeText(applicationContext, "History", Toast.LENGTH_SHORT).show()
                        drawer.closeDrawer(GravityCompat.START)
                    }
                    R.id.menu_about -> {
                        appTitle.text = "About"
                        Toast.makeText(applicationContext, "About", Toast.LENGTH_SHORT).show()
                        drawer.closeDrawer(GravityCompat.START)
                    }

                }

                return false
            }

        })

//        homeBottom.setOnClickListener {
//
//            var homeFragment = HomeFragment()
//            supportFragmentManager.beginTransaction().replace(R.id.mainContener, homeFragment, "")
//                .commit()
//
//            homeEl.visibility=View.VISIBLE
//            popularEl.visibility=View.INVISIBLE
//            randomEl.visibility=View.INVISIBLE
//            likedEl.visibility=View.INVISIBLE
//            appTitle.text="Home"
//            search.setImageResource(R.drawable.ic_sraech)
//            search.visibility=View.VISIBLE
//
//        }

//        popularBottom.setOnClickListener {
//            var popularFragment = PopularFragment()
//            supportFragmentManager.beginTransaction().replace(R.id.mainContener, popularFragment, "")
//                .commit()
//
//
//            homeEl.visibility=View.INVISIBLE
//            popularEl.visibility=View.VISIBLE
//            randomEl.visibility=View.INVISIBLE
//            likedEl.visibility=View.INVISIBLE
//            appTitle.text="Popular"
//            search.setImageResource(R.drawable.ic_sraech)
//            search.visibility=View.VISIBLE
//        }
//        randomBottom.setOnClickListener {
//
//            var randomFragment = RandomFragment()
//            supportFragmentManager.beginTransaction().replace(R.id.mainContener, randomFragment, "")
//                .commit()
//
//            homeEl.visibility=View.INVISIBLE
//            popularEl.visibility=View.INVISIBLE
//            randomEl.visibility=View.VISIBLE
//            likedEl.visibility=View.INVISIBLE
//            appTitle.text="Random"
//            search.setImageResource(R.drawable.ic_random)
//            search.visibility=View.VISIBLE
//        }

//        likedBottom.setOnClickListener {
//            var favouritesFragment =MyFavouritesFragment()
//            supportFragmentManager.beginTransaction().replace(R.id.mainContener, favouritesFragment, "")
//                .commit()
//
//            homeEl.visibility=View.INVISIBLE
//            popularEl.visibility=View.INVISIBLE
//            randomEl.visibility=View.INVISIBLE
//            likedEl.visibility=View.VISIBLE
//            appTitle.text="My Favourites"
//            search.visibility=View.INVISIBLE
//        }
    }

    private val selectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menuBottom_home -> {
                    var homeFragment = HomeFragment()
                    supportFragmentManager.beginTransaction().replace(
                        R.id.mainContener,
                        homeFragment,
                        ""
                    )
                        .commit()
                    appTitle.text = "Home"
                    menuBottom_home.setIcon(R.drawable.ic_home)
                    menuBottom_popular.setIcon(R.drawable.ic_popular1)
                    menuBottom_random.setIcon(R.drawable.ic_random)
                    menuBottom_liked.setIcon(R.drawable.ic_liked1)

                    search.visibility = View.VISIBLE
                    random.visibility = View.INVISIBLE
                    return@OnNavigationItemSelectedListener true
                }
                R.id.menuBottom_popular -> {
                    var popularFragment = PopularFragment()
                    supportFragmentManager.beginTransaction().replace(
                        R.id.mainContener,
                        popularFragment,
                        ""
                    )
                        .commit()
                    appTitle.text = "Popular"

                    search.visibility = View.VISIBLE
                    random.visibility = View.INVISIBLE
                    menuBottom_home.setIcon(R.drawable.ic_home1)
                    menuBottom_popular.setIcon(R.drawable.ic_popular)
                    menuBottom_random.setIcon(R.drawable.ic_random)
                    menuBottom_liked.setIcon(R.drawable.ic_liked1)

                    return@OnNavigationItemSelectedListener true
                }
                R.id.menuBottom_random -> {
                    var randomFragment = RandomFragment()
                    supportFragmentManager.beginTransaction().replace(
                        R.id.mainContener,
                        randomFragment,
                        ""
                    )
                        .commit()
                    appTitle.text = "Random"
                    search.visibility = View.INVISIBLE
                    random.visibility = View.VISIBLE
                    menuBottom_home.setIcon(R.drawable.ic_home1)
                    menuBottom_popular.setIcon(R.drawable.ic_popular1)
                    menuBottom_random.setIcon(R.drawable.ic_random1)
                    menuBottom_liked.setIcon(R.drawable.ic_liked1)

                    return@OnNavigationItemSelectedListener true
                }
                R.id.menuBottom_liked -> {
                    var favouritesFragment = MyFavouritesFragment()
                    supportFragmentManager.beginTransaction().replace(
                        R.id.mainContener,
                        favouritesFragment,
                        ""
                    )
                        .commit()
                    appTitle.text = "My Favourites"
                    search.visibility = View.INVISIBLE
                    random.visibility = View.INVISIBLE
                    menuBottom_home.setIcon(R.drawable.ic_home1)
                    menuBottom_popular.setIcon(R.drawable.ic_popular1)
                    menuBottom_random.setIcon(R.drawable.ic_random)
                    menuBottom_liked.setIcon(R.drawable.ic_liked)

                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

}