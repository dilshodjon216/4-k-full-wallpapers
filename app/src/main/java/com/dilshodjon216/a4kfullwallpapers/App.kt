package com.dilshodjon216.a4kfullwallpapers

import android.app.Application
import androidx.room.Room
import com.dilshodjon216.a4kfullwallpapers.dataBase.database.AppDatabase

class App : Application() {
    companion object {
        lateinit var instance: App
    }

    lateinit var appDatabase: AppDatabase
    override fun onCreate() {
        super.onCreate()
        instance = this
        appDatabase =
            Room.databaseBuilder(this, AppDatabase::class.java, "myDb.db").build()
    }
}