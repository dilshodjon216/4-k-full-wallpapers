package com.dilshodjon216.a4kfullwallpapers.fragment


import android.app.WallpaperManager
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.dilshodjon216.a4kfullwallpapers.R
import kotlinx.android.synthetic.main.fragment_apply_image.view.*
import java.io.IOException


class ApplyImageFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view=inflater.inflate(R.layout.fragment_apply_image, container, false)


        var image = arguments!!.getString("image")
        var author = arguments!!.getString("author")
        var url = arguments!!.getString("url")
        var width = arguments!!.getString("width")
        var height = arguments!!.getString("height")
        var imageOrginal = arguments!!.getString("imageOrginal")
        var imagePortarit = arguments!!.getString("imagePortarit")


        Glide.with(this).load(image).placeholder(R.drawable.img).into(view.image_apply)

        view.applyHome.setOnClickListener {
            setWallpaper(view.image_apply,"1")
        }
        view.applych.setOnClickListener {
            setWallpaper(view.image_apply,"2")
        }

        view.apply.setOnClickListener {
            setWallpaper(view.image_apply,"3")
        }


        view.applay_exit.setOnClickListener {
            var popularFragment = ImageFragment()

            var bundle = Bundle()
            bundle.putString("image", image)
            bundle.putString("author", author)
            bundle.putString("url", url)
            bundle.putString("width", width)
            bundle.putString("height", height)
            bundle.putString("imageOrginal", imageOrginal)
            bundle.putString("imagePortarit", imagePortarit)

            popularFragment.arguments = bundle

            fragmentManager!!.beginTransaction().replace(R.id.image_contener, popularFragment, "")
                .commit()
        }

        return view
    }

    private fun setWallpaper(imageView: ImageView,a:String){
        val bitmapDrawable = imageView.drawable as BitmapDrawable
        val bitmap = bitmapDrawable.bitmap

        var manager= WallpaperManager.getInstance(context!!)

        try {

            if(a == "1") {
                manager.setBitmap(bitmap)
                Toast.makeText(context!!, "Wallpaper set HomeScreen", Toast.LENGTH_SHORT).show()
            }else if(a=="2"){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    manager.setBitmap(bitmap,null,true,WallpaperManager.FLAG_LOCK);//For Lock screen
                    Toast.makeText(context, "Wallpaper set LockScreen", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(context, "Lock screen walpaper not supported",
                        Toast.LENGTH_SHORT).show();
                }
            }else{
                manager.setBitmap(bitmap)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    manager.setBitmap(bitmap,null,true,WallpaperManager.FLAG_LOCK);//For Lock screen

                }
                Toast.makeText(context, "Wallpaper set LockScreen and HomeScreen", Toast.LENGTH_SHORT).show();


            }

        } catch (e: IOException) {
            Toast.makeText(context!!, "Error!", Toast.LENGTH_SHORT).show()
        }
    }

}