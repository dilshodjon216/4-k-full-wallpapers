@file:Suppress("DEPRECATION")

package com.dilshodjon216.a4kfullwallpapers.fragment

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.*
import android.os.StrictMode.VmPolicy
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.dilshodjon216.a4kfullwallpapers.App
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.dataBase.Dao.LikesDao
import com.dilshodjon216.a4kfullwallpapers.dataBase.entity.Likes
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_image.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class ImageFragment : Fragment() {


    private var likesDao: LikesDao? = null
    var testLike = false
    lateinit var imageLike:ImageView
    lateinit var likes: Likes
    private val PERMISSION_REQUEST_CODE = 100
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        var view = inflater.inflate(R.layout.fragment_image, container, false)

        var image = arguments!!.getString("image")
        var author = arguments!!.getString("author")
        var url = arguments!!.getString("url")
        var width = arguments!!.getString("width")
        var height = arguments!!.getString("height")
        var imageOrginal = arguments!!.getString("imageOrginal")
        var imagePortarit = arguments!!.getString("imagePortarit")


         imageLike=view.likesBtn

        chekImageLike(image!!)

        likesDao = App.instance.appDatabase.likesDao()



        Glide.with(this).load(image).placeholder(R.drawable.img).into(view.imageBackground)

        view.image_info.setOnClickListener {
            var bottomSheetDialog = BottomSheetDialog(context!!, R.style.BottomSheetDialogTheme)

            var bottomSheetView = LayoutInflater.from(context).inflate(
                R.layout.layout_bottom_sheet,
                activity!!.findViewById(R.id.bottomSheetContainer),
            )

            bottomSheetView.findViewById<TextView>(R.id.size).text = width + "x" + height
            bottomSheetView.findViewById<TextView>(R.id.author).text = author
            bottomSheetView.findViewById<TextView>(R.id.urlWeb).text = url

            bottomSheetView.findViewById<TextView>(R.id.urlWeb).setOnClickListener {
                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(browserIntent)
            }

            bottomSheetDialog.setContentView(bottomSheetView)
            bottomSheetDialog.show()

        }

        view.likesBtn.setOnClickListener {

            if (!testLike) {
                insetUrl1(Likes(imagesUrl = image,with = width!!,height = height!!,photographer = author!!,photographer_url =url!! ))



            } else {
              deleteUrl(likes)
            }
        }



        view.image_shareit.setOnClickListener {
            val bitmapDrawable = view.imageBackground.drawable as BitmapDrawable
            val bitmap = bitmapDrawable.bitmap

            shareImageUri(saveImage(bitmap)!!)

        }

        view.imageApply.setOnClickListener {
            var popularFragment = ApplyImageFragment()

            var bundle = Bundle()
            bundle.putString("image", image)
            bundle.putString("author", author)
            bundle.putString("url", url)
            bundle.putString("width", width)
            bundle.putString("height", height)
            bundle.putString("imageOrginal", imageOrginal)
            bundle.putString("imagePortarit", imagePortarit)

            popularFragment.arguments = bundle


            fragmentManager!!.beginTransaction().replace(
                R.id.image_contener,
                popularFragment,
                ""
            ).addToBackStack(popularFragment.toString()).commit()

        }




        view.save_image.setOnClickListener {
            saveToGallery(view.imageBackground, author!!)
            Toast.makeText(context, "Image saved successfully", Toast.LENGTH_SHORT).show()
            view.save_image.setColorFilter(
                ContextCompat.getColor(context!!, R.color.colorIcon1),
                android.graphics.PorterDuff.Mode.SRC_IN
            );
        }

        view.imge_exit.setOnClickListener {
            activity!!.finish()
        }

        view.image_editor.setOnClickListener {
            var popularFragment = PhotoEditorFragment()

            var bundle = Bundle()
            bundle.putString("image", image)
            bundle.putString("author", author)
            bundle.putString("url", url)
            bundle.putString("width", width)
            bundle.putString("height", height)
            bundle.putString("imageOrginal", imageOrginal)
            bundle.putString("imagePortarit", imagePortarit)

            popularFragment.arguments = bundle


            fragmentManager!!.beginTransaction().replace(
                R.id.image_contener,
                popularFragment,
                ""
            ).addToBackStack(popularFragment.toString()).commit()

        }

        return view
    }


    private fun saveToGallery(imageView: ImageView, string: String) {
        val bitmapDrawable = imageView.drawable as BitmapDrawable
        val bitmap = bitmapDrawable.bitmap

        var state=Environment.getExternalStorageState()

        if(Environment.MEDIA_MOUNTED == state){
            if(Build.VERSION.SDK_INT>=23){
                if(checkPermission()){
                    var path=Environment.getExternalStorageDirectory().toString()
                    var file=File(path, "$string.jpg")
                    if(!file.exists()){
                        try {
                            val fos = FileOutputStream(file)
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                            fos.flush()
                            fos.close()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }else{
                    requestPermission();
                }
            }else{
                var path=Environment.getExternalStorageDirectory().toString();
                var file=File(path, "$string.jpg");
                if(!file.exists()){
                    var fos: FileOutputStream? = null
                    try {
                        fos = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                        fos.flush()
                        fos.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDetach() {
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        super.onDetach()
    }

    @Suppress("DEPRECATION")
    fun chekImageLike(images: String){
        class GetImageLike: AsyncTask<Void, Void, Likes>() {
            override fun doInBackground(vararg p0: Void?): Likes {
                return App.instance.appDatabase.likesDao().getLikesPhoto(images)
            }

            override fun onPostExecute(result: Likes?) {
                testLike = if(result!=null){
                    imageLike.setImageResource(R.drawable.ic_liked)
                    likes=result
                    true
                }else{
                    imageLike.setImageResource(R.drawable.ic_liked1)
                    false
                }
            }

        }
        GetImageLike().execute()
    }


    private fun saveImage(image: Bitmap): Uri? {
        val imagesFolder= File(activity!!.cacheDir, "images")
        var uri: Uri? = null
        try {
            imagesFolder.mkdirs()
            val file = File(imagesFolder, "shared_image.png")
            val stream = FileOutputStream(file)
            image.compress(Bitmap.CompressFormat.PNG, 90, stream)
            stream.flush()
            stream.close()
            uri = FileProvider.getUriForFile(context!!, "com.dilshodjon216.a4kfullwallpapers", file)
        } catch (e: IOException) {
            Log.d("sss->", "IOException while trying to write file for sharing: " + e.message)
        }
        return uri
    }

    private fun shareImageUri(uri: Uri) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.type = "image/png"
        startActivity(intent)
    }



    @Suppress("DEPRECATION")
    private fun insetUrl1(likes: Likes){

        class GetImageLike: AsyncTask<Void, Void, Boolean>(){
            override fun doInBackground(vararg p0: Void?): Boolean {
                App.instance.appDatabase.likesDao().insertLikes(likes)
                return true
            }
            override fun onPostExecute(result: Boolean?) {
                imageLike.setImageResource(R.drawable.ic_liked)
                testLike=true
            }

        }

        GetImageLike().execute()
    }

    private fun deleteUrl(likes: Likes){
        class GetImageLike: AsyncTask<Void, Void, Boolean>(){
            override fun doInBackground(vararg p0: Void?): Boolean {
                App.instance.appDatabase.likesDao().deleteLikes(likes)
                return true
            }
            override fun onPostExecute(result: Boolean?) {
                imageLike.setImageResource(R.drawable.ic_liked1)
                testLike=false
            }

        }

        GetImageLike().execute()
    }

    private fun checkPermission():Boolean {
        var result = ContextCompat.checkSelfPermission(
            activity!!,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        );
        return result == PackageManager.PERMISSION_GRANTED
    }


    private fun requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            )) {
            Toast.makeText(
                context,
                "Write External Storage permission allows us to save files. Please allow this permission in App Settings.",
                Toast.LENGTH_LONG
            ).show();
        } else {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE
            );
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
            }
        }
    }
}