package com.dilshodjon216.a4kfullwallpapers.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.adaptor.ImageAdaptor
import com.dilshodjon216.a4kfullwallpapers.model.Images
import com.dilshodjon216.a4kfullwallpapers.model.Photo
import com.dilshodjon216.a4kfullwallpapers.paging.PaginationListener1
import com.dilshodjon216.a4kfullwallpapers.paging.PaginationListener1.Paging.PAGE_SIZE
import com.dilshodjon216.a4kfullwallpapers.paging.PaginationListener1.Paging.PAGE_START
import com.dilshodjon216.a4kfullwallpapers.serviecs.ApiClient
import com.dilshodjon216.a4kfullwallpapers.serviecs.ApiService
import kotlinx.android.synthetic.main.fragment_popular.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PopularFragment : Fragment() {

    lateinit var imageAdaptor: ImageAdaptor
    lateinit var photoList: ArrayList<Photo>
    var current_page =PAGE_START
    var isLastPage = false
    var isLoading = false
    lateinit var query: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       var view=inflater.inflate(R.layout.fragment_popular, container, false)

        photoList = ArrayList()
        var gridLayoutManager = GridLayoutManager(context, 3)
        view.popularRV.layoutManager = gridLayoutManager
        imageAdaptor = ImageAdaptor(photoList, context!!)

        view.popularRV.adapter = imageAdaptor

        query = "popular"!!


        laodPhotos(query, current_page)

        view.popularRV.addOnScrollListener(object : PaginationListener1(gridLayoutManager) {
            override fun loadMoreItems() {
                isLoading = true
                current_page++
                laodPhotos(query, current_page)
            }

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

        })

        return view
    }

    private fun laodPhotos(query: String, page: Int) {

        var apiClinet = ApiClient.getRetrofit()
        var apiService = apiClinet.create(ApiService::class.java)

        apiService.getImage(query = query, page = page).enqueue(object : Callback<Images> {
            override fun onResponse(call: Call<Images>, response: Response<Images>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (current_page <= PAGE_SIZE) {
                        photoList.addAll(response.body()!!.photos)

                        imageAdaptor.removeLoading()

                        imageAdaptor.addItems(photoList)

                    }

                    if (current_page < PAGE_SIZE) {
                        imageAdaptor.addLoading()
                    } else {
                        isLastPage = true
                    }
                    isLoading = false
                }
            }

            override fun onFailure(call: Call<Images>, t: Throwable) {

            }

        })
    }

}