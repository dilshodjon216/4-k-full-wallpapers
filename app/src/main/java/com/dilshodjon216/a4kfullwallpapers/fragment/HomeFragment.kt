package com.dilshodjon216.a4kfullwallpapers.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.adaptor.PageViewAdaptor
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {


    lateinit var pagerViewAdaptor: PageViewAdaptor
    lateinit var title: ArrayList<String>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_home, container, false)

        loadData()
        pagerViewAdaptor = PageViewAdaptor(title, fragmentManager!!)
        view.viewPager.adapter = pagerViewAdaptor
        view.tabLayout.setupWithViewPager(view.viewPager)

        return view
    }

    private fun loadData() {
        title = java.util.ArrayList()
        title.add("Covid-19")
        title.add("Travel")
        title.add("Wallpapers")
        title.add("Animals")
        title.add("Technology")
        title.add("Nature")
        title.add("Style")
        title.add("Scenic")
        title.add("food")
        title.add("blue")
        title.add("red")
        title.add("green")
        title.add("art")
        title.add("design")
        title.add("beautiful")
        title.add("outdoors")
        title.add("creative")
        title.add("tea")
        title.add("cat")
        title.add("color")
        title.add("pink")
        title.add("lifestyle")
        title.add("world")
        title.add("woods")
        title.add("idea")
        title.add("trees")
        title.add("soccer")


    }


}