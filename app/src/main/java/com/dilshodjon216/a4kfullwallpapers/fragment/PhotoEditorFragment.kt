package com.dilshodjon216.a4kfullwallpapers.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.dilshodjon216.a4kfullwallpapers.R
import kotlinx.android.synthetic.main.fragment_photo_editor.view.*


class PhotoEditorFragment : Fragment(){




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view= inflater.inflate(R.layout.fragment_photo_editor, container, false)


        var image = arguments!!.getString("image")
        var author = arguments!!.getString("author")
        var url = arguments!!.getString("url")
        var width = arguments!!.getString("width")
        var height = arguments!!.getString("height")
        var imageOrginal = arguments!!.getString("imageOrginal")
        var imagePortarit = arguments!!.getString("imagePortarit")



     //   Glide.with(this).load(image).placeholder(R.drawable.img).into(view.edit)





        view.imge_exit2.setOnClickListener {
            var popularFragment = ImageFragment()

            var bundle = Bundle()
            bundle.putString("image", image)
            bundle.putString("author", author)
            bundle.putString("url", url)
            bundle.putString("width", width)
            bundle.putString("height", height)
            bundle.putString("imageOrginal", imageOrginal)
            bundle.putString("imagePortarit", imagePortarit)

            popularFragment.arguments = bundle

            fragmentManager!!.beginTransaction().replace(R.id.image_contener, popularFragment, "")
                .commit()
        }



        return view
    }


}