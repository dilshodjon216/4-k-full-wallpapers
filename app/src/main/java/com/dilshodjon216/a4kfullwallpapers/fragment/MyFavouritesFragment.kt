package com.dilshodjon216.a4kfullwallpapers.fragment

import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.dilshodjon216.a4kfullwallpapers.App
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.adaptor.FavouritesImageAdaptor
import com.dilshodjon216.a4kfullwallpapers.dataBase.Dao.LikesDao
import com.dilshodjon216.a4kfullwallpapers.dataBase.entity.Likes
import kotlinx.android.synthetic.main.fragment_my_favourites.view.*


@Suppress("DEPRECATION")
class MyFavouritesFragment : Fragment() {



    lateinit var list: ArrayList<Likes>
    lateinit var favouritesImageAdaptor: FavouritesImageAdaptor
    lateinit var likesDao:LikesDao
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_my_favourites, container, false)


        list= ArrayList()
        var gridLayoutManager = GridLayoutManager(context, 3)
        view.favourites_rv.layoutManager=gridLayoutManager
        favouritesImageAdaptor= FavouritesImageAdaptor(list,context!!)
        view.favourites_rv.adapter=favouritesImageAdaptor

        getLikes()

        return view
    }

    private fun getLikes() {

        class GetLikesImages: AsyncTask<Void, Void, ArrayList<Likes>>(){
            override fun doInBackground(vararg p0: Void?): ArrayList<Likes> {
                return App.instance.appDatabase.likesDao().getAll() as ArrayList<Likes>
            }

            override fun onPostExecute(result: ArrayList<Likes>?) {
                list.addAll(result!!)
                favouritesImageAdaptor.notifyItemInserted(list.size)
            }

        }

        GetLikesImages().execute()
    }


}