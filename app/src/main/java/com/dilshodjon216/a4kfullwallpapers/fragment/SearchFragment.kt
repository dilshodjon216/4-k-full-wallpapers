package com.dilshodjon216.a4kfullwallpapers.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.GridLayoutManager
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.adaptor.ImageAdaptor
import com.dilshodjon216.a4kfullwallpapers.model.Images
import com.dilshodjon216.a4kfullwallpapers.model.Photo
import com.dilshodjon216.a4kfullwallpapers.paging.PaginationListener
import com.dilshodjon216.a4kfullwallpapers.paging.PaginationListener.Paging.PAGE_START
import com.dilshodjon216.a4kfullwallpapers.serviecs.ApiClient
import com.dilshodjon216.a4kfullwallpapers.serviecs.ApiService
import kotlinx.android.synthetic.main.fragment_image_item.view.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SearchFragment : Fragment() {


    lateinit var imageAdaptor: ImageAdaptor
    lateinit var photoList: ArrayList<Photo>
    var current_page = PAGE_START
    var isLastPage = false
    var isLoading = false
    lateinit var query: String
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_search, container, false)

        var search=activity!!.findViewById<EditText>(R.id.searchET)

        photoList= ArrayList()
        var gridLayoutManager = GridLayoutManager(context, 3)
        view.searchRV.layoutManager = gridLayoutManager
        imageAdaptor=ImageAdaptor(photoList,context!!)
        view.searchRV.adapter=imageAdaptor

        search.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                photoList.clear()
                imageAdaptor=ImageAdaptor(photoList,context!!)
                view.searchRV.adapter=imageAdaptor
                query=p0.toString()
                laodPhotos(query,current_page)

            }

            override fun afterTextChanged(p0: Editable?) {
               // laodPhotos(query,current_page)
            }

        })

        view.searchRV.addOnScrollListener(object : PaginationListener(gridLayoutManager) {
            override fun loadMoreItems() {
                isLoading = true
                current_page++
                laodPhotos(query, current_page)
            }

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

        })

        return view

    }


    private fun laodPhotos(query: String, page: Int) {

        var apiClinet = ApiClient.getRetrofit()
        var apiService = apiClinet.create(ApiService::class.java)

        apiService.getImage(query = query, page = page).enqueue(object : Callback<Images> {
            override fun onResponse(call: Call<Images>, response: Response<Images>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (current_page <= PaginationListener.Paging.PAGE_SIZE) {

                        photoList.addAll(response.body()!!.photos)
                        imageAdaptor.removeLoading()
                        imageAdaptor.addItems(photoList)

                    }

                    if (current_page < PaginationListener.Paging.PAGE_SIZE) {
                        imageAdaptor.addLoading()
                    } else {
                        isLastPage = true
                    }
                    isLoading = false
                }
            }

            override fun onFailure(call: Call<Images>, t: Throwable) {

            }

        })
    }


}