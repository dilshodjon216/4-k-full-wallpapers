package com.dilshodjon216.a4kfullwallpapers.paging
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dilshodjon216.a4kfullwallpapers.paging.PaginationListener.Paging.PAGE_SIZE

abstract class PaginationListener(var layoutManager: GridLayoutManager): RecyclerView.OnScrollListener(){

    object Paging {
        var PAGE_START = 1
        var PAGE_SIZE = 15
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val childCount = layoutManager.childCount
        val itemCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        if (!isLastPage() && !isLoading()) {
            if ((childCount + firstVisibleItemPosition >= itemCount)
                && firstVisibleItemPosition >0
                && itemCount >= PAGE_SIZE
            ) {
                loadMoreItems()
            }
        }
    }

    abstract fun loadMoreItems()

    abstract fun isLastPage(): Boolean

    abstract fun isLoading(): Boolean
}