package com.dilshodjon216.a4kfullwallpapers.`interface`

import com.zomato.photofilters.imageprocessors.Filter

interface FiltersListFragmentListener {
    fun onFilterSelected(filter:Filter)
}