package com.dilshodjon216.a4kfullwallpapers.serviecs

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    private const val BASE_URL="https://api.pexels.com/v1/"
    const val API_KET="563492ad6f9170000100000152a24ed7bc6e4a279e8d026c50df89b9"
    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}