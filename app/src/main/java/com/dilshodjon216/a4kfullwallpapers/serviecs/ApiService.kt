package com.dilshodjon216.a4kfullwallpapers.serviecs

import com.dilshodjon216.a4kfullwallpapers.model.Images
import com.dilshodjon216.a4kfullwallpapers.serviecs.ApiClient.API_KET
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {


    @GET("search")
    fun getImage(
        @Header("Authorization") auth: String = API_KET,
        @Query("query") query: String,
        @Query("page") page: Int
    ): Call<Images>



}