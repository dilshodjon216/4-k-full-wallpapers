package com.dilshodjon216.a4kfullwallpapers.adaptor

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.activtiy.ImageActivity
import com.dilshodjon216.a4kfullwallpapers.model.Photo
import kotlinx.android.synthetic.main.item_image.view.*

class ImageAdaptor(var photolist: ArrayList<Photo>, var context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val DATA_ITEM_TYPE = 0
    private val PROGRESS_ITEM_TYPE = 1

    var isLoaderVisible = false
    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBand(photo: Photo) {
            Glide.with(context).load(photo.src.large).into(itemView.imageIV);

            itemView.imageIV.setOnClickListener {
                var intent=Intent(context,ImageActivity::class.java)
                intent.putExtra("url",photo.photographer_url)
                intent.putExtra("author",photo.photographer)
                intent.putExtra("width",photo.width.toString())
                intent.putExtra("height",photo.height.toString())
                intent.putExtra("imageLarge",photo.src.large2x)
                intent.putExtra("imageOrginal",photo.src.original)
                intent.putExtra("imagePortarit",photo.src.small)
                context.startActivity(intent)
            }
        }
    }

    inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == DATA_ITEM_TYPE) {
            VH(
                LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false)
            )
        } else {
            ProgressViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_progress, parent, false)
            )
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == DATA_ITEM_TYPE) {
            val vh = holder as VH
            vh.onBand(photolist[position])
        }
    }

    override fun getItemCount(): Int = photolist.size

    override fun getItemViewType(position: Int): Int {
        if (isLoaderVisible) {
            return if (position == photolist.size - 1) {
                PROGRESS_ITEM_TYPE
            } else {
                DATA_ITEM_TYPE
            }
        }
        return DATA_ITEM_TYPE
    }


    interface ImageOnClick {
        fun onClick(photo: Photo)
    }


    fun addItems(list: List<Photo>) {
        photolist.addAll(list)
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaderVisible = true
    }

    fun removeLoading() {
        isLoaderVisible = false
    }

}