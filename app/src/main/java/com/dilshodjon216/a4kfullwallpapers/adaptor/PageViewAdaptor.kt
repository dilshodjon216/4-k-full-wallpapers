package com.dilshodjon216.a4kfullwallpapers.adaptor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.dilshodjon216.a4kfullwallpapers.fragment.ImageItemFragment

class PageViewAdaptor(var titleString:ArrayList<String>,fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager){

    override fun getCount(): Int=titleString.size

    override fun getItem(position: Int): Fragment {
        return ImageItemFragment.newInstance(titleString[position])
    }


    override fun getPageTitle(position: Int): CharSequence? {
        return titleString[position].toUpperCase()
    }
}