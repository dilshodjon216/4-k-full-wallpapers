package com.dilshodjon216.a4kfullwallpapers.adaptor

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dilshodjon216.a4kfullwallpapers.R
import com.dilshodjon216.a4kfullwallpapers.activtiy.ImageActivity
import com.dilshodjon216.a4kfullwallpapers.dataBase.entity.Likes
import kotlinx.android.synthetic.main.item_image.view.*

class FavouritesImageAdaptor(var likesList: ArrayList<Likes>, var context: Context) :
    RecyclerView.Adapter<FavouritesImageAdaptor.Vh>() {

    inner class Vh(view: View) : RecyclerView.ViewHolder(view) {
        fun onBand(likes: Likes) {

            Glide.with(context).load(likes.imagesUrl).into(itemView.imageIV);

            itemView.imageIV.setOnClickListener {
                var intent= Intent(context, ImageActivity::class.java)
                intent.putExtra("url",likes.photographer_url)
                intent.putExtra("author",likes.photographer)
                intent.putExtra("width",likes.with.toString())
                intent.putExtra("height",likes.height.toString())
                intent.putExtra("imageLarge",likes.imagesUrl)
                intent.putExtra("imageOrginal","")
                intent.putExtra("imagePortarit","")
                context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Vh {
        return Vh(LayoutInflater.from(context).inflate(R.layout.item_image, parent, false))
    }

    override fun onBindViewHolder(holder: Vh, position: Int) {
       holder.onBand(likesList[position])
    }

    override fun getItemCount(): Int = likesList.size


}