package com.dilshodjon216.a4kfullwallpapers.adaptor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dilshodjon216.a4kfullwallpapers.R
import com.zomato.photofilters.imageprocessors.Filter
import com.zomato.photofilters.utils.ThumbnailItem
import kotlinx.android.synthetic.main.thumbnail_item.view.*

class ThumbnailAdaptor(var thumbnailItems: List<ThumbnailItem>, var context: Context,var filtersListFragmentListener: FiltersListFragmentListener) :
    RecyclerView.Adapter<ThumbnailAdaptor.VH>() {

    private var selectedIndex=0
    inner class VH(view: View) : RecyclerView.ViewHolder(view) {
        fun onBand(thumbnailItem: ThumbnailItem,position: Int) {
            itemView.thumbnail.setImageBitmap(thumbnailItem.image)
            itemView.filter_name.text=thumbnailItem.filterName
            itemView.thumbnail.setOnClickListener {
                filtersListFragmentListener.onFilterSelected(thumbnailItem.filter)
                selectedIndex=position
                notifyDataSetChanged()
            }
            if(selectedIndex==position){
                itemView.filter_name.setTextColor(ContextCompat.getColor(context,R.color.colorIcon1))
            }else{
                itemView.filter_name.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(context).inflate(R.layout.thumbnail_item, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBand(thumbnailItems[position],position)
    }

    override fun getItemCount(): Int = thumbnailItems.size

    interface FiltersListFragmentListener {
        fun onFilterSelected(filter: Filter)
    }
}